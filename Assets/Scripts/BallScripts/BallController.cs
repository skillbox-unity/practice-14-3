using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class BallController : MonoBehaviour
{
    private Rigidbody ballRigidbody;
    public CameraController cameraController;
    [SerializeField] private int power;

    private void Start()
    {
        ballRigidbody = GetComponent<Rigidbody>();
    }

    public void moveBall(Vector3 movement)
    {
        ballRigidbody.AddForce(movement * power);
        cameraController.moveCamera(transform.position);
    }
}
