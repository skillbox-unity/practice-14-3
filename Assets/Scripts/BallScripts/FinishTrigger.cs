using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Game.Inputs;

public class FinishTrigger : MonoBehaviour
{
    [SerializeField] private GameObject winMenu;
    [SerializeField] private GameObject winEffectsParticleSystemObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            if (SceneManager.GetActiveScene().buildIndex + 1 != GlobalVars.MAX_LEVEL)
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            else
            {
                if (winMenu != null && winEffectsParticleSystemObject != null) {
                    winMenu.SetActive(true);
                    winEffectsParticleSystemObject.SetActive(true);
                }
            }
        }
    }
}
