using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallTrigger : MonoBehaviour
{

    [SerializeField] private GameObject fallenObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gameObject.SetActive(false);
            Rigidbody fallenObjectRigidbody = fallenObject.GetComponent<Rigidbody>();
            fallenObjectRigidbody.isKinematic = false;
        }
    }
}
