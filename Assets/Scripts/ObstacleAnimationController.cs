using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleAnimationController : MonoBehaviour
{
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void generateNewAnaimation()
    {
        int animationNumber = Random.Range(1, 4);
        animator.SetInteger("animationNumber", animationNumber);
    }
}
