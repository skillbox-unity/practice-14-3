using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveObjectTrigger : MonoBehaviour
{
    [SerializeField] private GameObject actionObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            actionObject.SetActive(false);
        }
    }
}