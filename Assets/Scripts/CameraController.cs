using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector3 offset;

    private void Start()
    {
        offset = new Vector3(0, 6, 6);
    }

    public void moveCamera(Vector3 ballPosition)
    {
        transform.position = ballPosition + offset;
    }
}
