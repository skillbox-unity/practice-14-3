using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] private Animation doorAnimation;
    [SerializeField] private NearbyButtonTextController nearbyButtonTextController;

    void Update()
    {
        OnButtonPressed();
    }

    private void OnButtonPressed()
    {
        if (Input.GetKeyDown(KeyCode.E) && CheckTheDoor())
        {
            doorAnimation.Play();
            nearbyButtonTextController.isOpenDoor = true;
        }
    }

    private bool CheckTheDoor()
    {
        if (nearbyButtonTextController.isNearby && !nearbyButtonTextController.isOpenDoor)
            return true;
        else
            return false;
    }
}
