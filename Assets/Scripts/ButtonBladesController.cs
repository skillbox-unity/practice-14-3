using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBladesController : MonoBehaviour
{
    [SerializeField] private Animator bladesAnimator;
    [SerializeField] private NearbyButtonTextController nearbyButtonTextController;

    void Update()
    {
        OnButtonPressed();
    }

    private void OnButtonPressed()
    {
        if (Input.GetKeyDown(KeyCode.E) && nearbyButtonTextController.isNearby)
        {
            if (bladesAnimator.speed > 0)
                bladesAnimator.speed = 0;
            else
                bladesAnimator.speed = 1;
        }
    }

}
