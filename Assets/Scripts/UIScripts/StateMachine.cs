using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{

    [SerializeField] private GameObject playCanvas;
    [SerializeField] private GameObject levelsCanvas;

    private GameObject currentScreen;

    void Start()
    {
        playCanvas.SetActive(true);
        levelsCanvas.SetActive(false);
        currentScreen = playCanvas;
    }

    public void changeState(GameObject state)
    {
        if (currentScreen != null)
        {
            currentScreen.SetActive(false);
            state.SetActive(true);
            currentScreen = state;
        }
    }
}