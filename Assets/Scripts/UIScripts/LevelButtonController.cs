using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelButtonController : MonoBehaviour
{
    public int levelID;

    public void onLevelButtonClicked()
    {
        SceneManager.LoadScene(levelID);
    }
}
