using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusCatchController : MonoBehaviour
{
    [SerializeField] private GameObject bonusEffectCatch;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            bonusEffectCatch.SetActive(true);
            Destroy(gameObject);
        }
    }
}
