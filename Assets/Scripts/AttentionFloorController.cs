using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttentionFloorController : MonoBehaviour
{
    [SerializeField] private GameObject actionObject;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            actionObject.SetActive(true);
        }
        else
        {
            if (collision.gameObject.TryGetComponent<Rigidbody>(out Rigidbody fallenObjectRigidbody))
            {
                Vector3 direction = collision.transform.position - transform.position;
                collision.rigidbody.AddForceAtPosition(direction.normalized * 20, transform.position, ForceMode.Impulse);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            actionObject.SetActive(true);
        }
    }
}
